<?php
return [
  'id' => 'yiiapp',
  'basePath' => dirname(__DIR__),
  'components' => [
       'formatter' => [
            'locale' => 'en_AU',
        ],
  ]
];
